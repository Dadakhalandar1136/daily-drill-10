const fs = require('fs');
const path = require('path');

const pathFileOne = path.join(__dirname, './fileOne.txt');
const pathFileTwo = path.join(__dirname, './fileTwo.txt');
const lipsumFilePath = path.join(__dirname, './lipsum.txt');
const newLipsumFilePath = path.join(__dirname, './newLipsumFile.txt');
const filePathToSaveActivity = path.join(__dirname, './saveActivityData.txt');



// NOTE: Do not change the name of this file

// NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.


// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


function createFiles(pathFileOne, pathFileTwo) {
    return new Promise((resolve, reject) => {
        fs.writeFile(pathFileOne, JSON.stringify('Data stored in first file'), (error) => {
            if (error) {
                reject(new Error(error));
            } else {
                resolve("First file created successful");
            }
        });
        return new Promise((resolve, reject) => {
            fs.writeFile(pathFileTwo, JSON.stringify('Data stored in second file'), (error) => {
                if (error) {
                    reject(new Error(error));
                } else {
                    console.log("Second file created successful");
                    resolve();
                }
            });
        });
    });
}

function deleteCreatedFiles(createdPathFile) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            fs.unlink(createdPathFile, (error) => {
                if (error) {
                    reject(new Error(error));
                } else {
                    resolve('Created file has been deleted successfully');
                }
            });
        }, 2 * 1000);
    });
}

function problemOne() {
    createFiles(pathFileOne, pathFileTwo)
        .then((message) => {
            console.log(message);
        })
        .then(() => {
            return deleteCreatedFiles(pathFileOne);
        })
        .then((message) => {
            console.log(message);
            return deleteCreatedFiles(pathFileTwo);
        })
        .then((message) => {
            console.log(message);
        })
        .catch((error) => {
            console.error(error);
        })
}


// Q2. Create a new file with lipsum data (you can google and get this). 
// Do File Read and write data to another file
// Delete the original file 
// Using promise chaining

function readLipsumData(lipsumFilePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(lipsumFilePath, (error, data) => {
            if (error) {
                reject(new Error(error));
            } else {
                console.log("Lisum data read successful");
                resolve(data);
            }
        });
    });
}

function writeNewLipsumFile(newLipsumFilePath, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(newLipsumFilePath, data, (error) => {
            if (error) {
                reject(new Error(error));
            } else {
                resolve('New Lipsum file created');
            }
        });
    });
}

function originalFileDelete(lipsumFilePath) {
    return new Promise((resolve, reject) => {
        fs.unlink(lipsumFilePath, (error) => {
            if (error) {
                reject(new Error(error));
            } else {
                resolve("Original lipsum file deleted successfully");
            }
        });
    });
}

function problemTwo() {
    readLipsumData(lipsumFilePath)
        .then((data) => {
            return writeNewLipsumFile(newLipsumFilePath, data);
        })
        .then((message) => {
            console.log(message);
            return originalFileDelete(lipsumFilePath);
        })
        .then((message) => {
            console.log(message);
        })
        .catch((error) => {
            console.error(error);
        })
}



// Q3.
// Use appropriate methods to 
// A. login with value 3 and call getData once login is successful
// B. Write logic to logData after each activity for each user. Following are the list of activities
//     "Login Success"
//     "Login Failure"
//     "GetData Success"
//     "GetData Failure"

//     Call log data function after each activity to log that activity in the file.
//     All logged activity must also include the timestamp for that activity.

//     You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
//     All calls must be chained unless mentioned otherwise.

// */

function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
    const data = JSON.stringify(
        {
            "time stamp": new Date().toString(),
            [user]: activity
        }
    );

    return new Promise((resolve, reject) => {
        fs.appendFile(filePathToSaveActivity, "\n" + data, (error) => {
            if (error) {
                reject(new Error(error));
            } else {
                resolve();
            }
        });
    });
}

function userLogin(user, val) {
    login(user, val)
        .then((user) => {
            logData(user, "Login Success");
            return getData();
        })
        .then((data) => {
            console.log("data fetched", data);
            logData(user, "GetData Success");
        })
        .catch((error) => {
            if (error.message.includes("User not found")) {
                logData(user, "Login Failure");
            } else {
                logData(user, "GetData Failure");
            }
        });
}


// problemOne();

problemTwo();

// userLogin('dadu', 3);
// userLogin('dadu', 4);

